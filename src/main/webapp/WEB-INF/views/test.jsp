<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>JSP Hello World</title>
</head>
<body>
<h1>Hello</h1>
<p>Welcome, user from <%= request.getRemoteAddr() %>
<p>It's now <%= new SimpleDateFormat("MM/dd/yyyy HH:mm").format(new Date()) %>
</body>
</html>