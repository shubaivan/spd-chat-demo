<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>


<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1">
<title>Pagina Principal</title>
</head>
 
<body>
	
	<form action='<spring:url value="/loginAction"/>' method="post">
		<input type="text" name="username" autocomplete="off">
		<input type="password" name="password" autocomplete="off">
		<button type="submit">Sign In</button>
	</form>

	<a href="${contextPath}/registration">Registration</a>

</body>
</html>