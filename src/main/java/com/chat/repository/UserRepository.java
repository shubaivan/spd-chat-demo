package com.chat.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.chat.model.User;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Repository
public class UserRepository implements UserRepositoryInterface {

  @Autowired
  private SessionFactory sessionFactory;

  private Session getSession() {
    return sessionFactory.getCurrentSession();
  }

  public User getByUserName(String username) {

    Query query = getSession().createQuery("select c.enabled,c.username from User c");
    List<Object[]> rows = query.list();

    return (User) getSession().createQuery(
            "from User where username = :username")
            .setParameter("username", username)
            .uniqueResult();
  }

  public User getByEmail(String email) {
    return (User) getSession().createQuery(
            "from User where email = :email")
            .setParameter("email", email)
            .uniqueResult();
  }

  public void save(User user) {
    getSession().save(user);
  }

  public void persist(User user) {
    getSession().persist(user);
  }

  public List getAll() {
    return getSession().createQuery("from User").list();
  }
}
