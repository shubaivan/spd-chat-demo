package com.chat.repository;

import com.chat.model.User;

import java.util.List;

public interface UserRepositoryInterface {
  User getByUserName(String username);
  User getByEmail(String email);
  void save(User user);
  void persist(User user);
  public List getAll();
}
