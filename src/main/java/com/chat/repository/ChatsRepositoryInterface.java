package com.chat.repository;

import com.chat.model.Chat;

import java.util.List;

public interface ChatsRepositoryInterface {

    void delete(Chat chat);

    void save(Chat chat);

    public List getAll();
}
