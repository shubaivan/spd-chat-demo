package com.chat.repository;

import com.chat.model.Authorities;

public interface AuthoritiesRepositoryInterface {
    Authorities getByAuthority(String authority);
}
