package com.chat.repository;

import com.chat.model.Authorities;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public class AuthoritiesRepository implements AuthoritiesRepositoryInterface {

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    public Authorities getByAuthority(String authority) {
        return (Authorities) getSession().createQuery(
                "from Authorities where authority = :authority")
                .setParameter("authority", authority)
                .uniqueResult();
    }
}
