package com.chat.repository;

import com.chat.model.Chat;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class ChatsRepository implements ChatsRepositoryInterface {

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    public void save(Chat chat) {
        getSession().save(chat);
    }

    public void delete(Chat chat) {
        getSession().delete(chat);
    }

    public List getAll() {
        return getSession().createQuery("from Chat").list();
    }
}
