package com.chat.controller;

import java.security.Principal;
import java.util.Date;
import java.util.List;

import com.chat.repository.AuthoritiesRepositoryInterface;
import com.chat.repository.ChatsRepositoryInterface;
import com.chat.repository.UserRepositoryInterface;
import com.chat.model.Authorities;
import com.chat.model.Chat;
import com.chat.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class MainContoller {

    @Autowired
    private UserRepositoryInterface userRepositoryInterface;

    @Autowired
    private ChatsRepositoryInterface chatsRepositoryInterface;

    @Autowired
    private AuthoritiesRepositoryInterface authoritiesRepositoryInterface;

    private static final Logger logger = LoggerFactory
            .getLogger(MainContoller.class);

    @GetMapping("/")
    public ModelAndView index(Model model, Principal principal) {
//        model.addAttribute("message", "You are logged in as " + principal.getName());
//        return "index";

        return new ModelAndView("redirect:" + "chats");
    }

    @GetMapping("/test")
    public String indexTest(Model model, Principal principal) {
        User user = userRepositoryInterface.getByUserName("test");

        model.addAttribute("message", "You are logged in as " + principal.getName());
        return "test";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public ModelAndView loginPage() {
        return new ModelAndView("registration");
    }

    @RequestMapping(value = "/create-user", method = RequestMethod.POST)
    public ModelAndView createUser(
            @RequestParam String name,
            @RequestParam String email,
            @RequestParam String password,
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        try {
            User user = new User();
            user.setUsername(name);
            user.setEmail(email);
            user.setEnabled(true);
            user.setTimestamp(new Date().getTime());
            String encoded = new BCryptPasswordEncoder().encode(password);
            user.setPassword(encoded);

            if (userRepositoryInterface.getByEmail(email) == null) {
                Authorities authoritie = new Authorities();
                authoritie.setUser(user);
                authoritie.setAuthority("ROLE_ADMIN");

                userRepositoryInterface.persist(user);
            }
        } catch (Exception e) {
            logger.error("Exception in creating user: ", e.getStackTrace(), e.getMessage());
        }

        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/chats", method = RequestMethod.GET)
    public ModelAndView chatsPage() {
        return new ModelAndView("chats");
    }

    @ResponseBody
    @RequestMapping(value = "/get-all-chats", method = RequestMethod.GET)
    public List getAllChats() {
        try {
            return chatsRepositoryInterface.getAll();
        } catch (Exception e) {
            logger.error("Exception in fetching chats: ", e.getStackTrace());
        }
        return null;
    }

    @ResponseBody
    @RequestMapping(value = "/get-all-users", method = RequestMethod.GET)
    public List getAllUsers() {
        try {
            return userRepositoryInterface.getAll();
        } catch (Exception e) {
            logger.error("Exception in fetching users: ", e.getStackTrace());
        }
        return null;
    }

    @ResponseBody
    @RequestMapping(value = "/post-chat", method = RequestMethod.POST)
    public ModelAndView postChat(
            Principal principal,
            @RequestParam String message) {
        try {
            User user = userRepositoryInterface.getByUserName(principal.getName());

            Chat chat = new Chat();
            chat.setMessage(message);
            chat.setUser(user);
            chat.setTimestamp(new Date().getTime());

            chatsRepositoryInterface.save(chat);
        } catch (Exception e) {
            logger.error("Exception in saving chat: ", e.getStackTrace());
        }
        return null;
    }
}
