package com.chat.model;

import javax.persistence.*;

@Entity
@Table(name = "authorities")
public class Authorities {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @Column(name = "authority")
  private String authority;

  @ManyToOne
  @JoinColumn(name = "username")
  private User user;

  public String getAuthority() {
    return authority;
  }

  public void setAuthority(String authority) {
    this.authority = authority;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    user.addAuthorities(this);
    this.user = user;
  }


}
