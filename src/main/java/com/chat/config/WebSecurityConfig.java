package com.chat.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService userDetailsService;

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	};

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
				.csrf().disable()
				.authorizeRequests()
				.antMatchers("/registration*").anonymous()
				.antMatchers("/create-user*").anonymous()

				.anyRequest().hasAnyRole("ADMIN", "USER")
				.antMatchers("/test/**").access("hasRole('ADMIN')")
				.antMatchers("/post-chat/*").access("hasRole('ADMIN')")
				.antMatchers("/get-all-users/*").access("hasRole('ADMIN')")
				.antMatchers("/get-all-chats/*").access("hasRole('ADMIN')")
				.antMatchers("/chats/*").access("hasRole('ADMIN')")

				.and()
				.authorizeRequests().antMatchers("/login**").permitAll()

				.and()
				.formLogin().loginPage("/login").loginProcessingUrl("/loginAction").permitAll()
				.and()
				.logout().logoutSuccessUrl("/login").permitAll();
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/static/**");
	}

}
